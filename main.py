import praw
from dotenv import load_dotenv
import os
import re

def get_new_subreddit_posts(reddit:praw.reddit.Reddit, subreddit:str, limit:int):
    ''''''
    submission_list = [
        submission for submission in reddit.subreddit(subreddit).new(limit=limit)
    ]

    return submission_list

if __name__ == "__main__":
    ''''''

    # load environmental variables
    load_dotenv()
    
    # grab credentials from env vars
    client_id = os.getenv('CLIENT_ID')
    client_secret = os.getenv('CLIENT_SECRET')
    user_agent = os.getenv('USER_AGENT')

    # authenticate with reddit
    reddit = praw.Reddit(
        client_id=client_id,
        client_secret=client_secret,
        user_agent=user_agent,
    )

    # grab user input
    subreddit = input("Which subreddit?")
    pattern_string = input("Pattern?")

    # get subreddit posts
    submissions_list = get_new_subreddit_posts(
        reddit=reddit,
        subreddit=subreddit, 
        limit=100)

    # search for regex pattern
    pattern = rf".*({pattern_string}).*"

    # find submissions that match the regex pattern
    submissions_matching_pattern = [
        submission for submission in submissions_list if re.search(pattern, submission.selftext)
    ]

    for submission in submissions_matching_pattern:
        print("=============================")
        print(f"Title : {submission.title}\n")
        print(f"Text : {submission.selftext}\n")
        print(f"URL : {submission.url}")
        print("=============================")

    # for submission in submissions_list:
    #     if re.search(pattern, submission.selftext):
    #         submissions_matching_pattern.append(submission)


    # send email

    # for submission in reddit.subreddit("ProgrammerHumor").hot(limit=20):
    #     print(submission.title)
