# REDDIT-SCANNER
---
> Scripting to scan subreddits for regex patterns


## Setup
---
> Python Setup

```
# create virtual environment
python3 -m venv venv

# activate virtual env
source venv/bin/activate

# install requirements
pip3 install -r requirements.txt
```

> Credentials
Create `.env` file with credentials
```
CLIENT_ID=example_client_id
CLIENT_SECRET=example_secret_id
USER_AGENT=example_user_agent
```

## Running
---
> How to run the script

```
python3 main.py

# answer the prompts
Which subreddit?ExampleSubreddit

Pattern?Example


# output below, will include title, text body and url.
```

## References
---
- https://www.w3schools.com/python/python_regex.asp
- https://regex101.com/
- https://praw.readthedocs.io/en/stable/getting_started/quick_start.html